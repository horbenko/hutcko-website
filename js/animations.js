'use strict';


var underlineAnimation = document.querySelector('.underline_animation');

if(typeof(underlineAnimation) != 'undefined' && underlineAnimation != null){
  window.addEventListener('scroll', function(e) {
    document.querySelectorAll('.underline_animation').forEach(item => {
      if( isScrolledIntoView(item) ){
        item.classList.add('showed');
      }
    });
  });
}



var countUp = document.querySelector('.js-countUp');

if(typeof(countUp) != 'undefined' && countUp != null){
  window.addEventListener('scroll', function(e) {
    document.querySelectorAll('.js-countUp').forEach(item => {
      if( isScrolledIntoView(item) ){
        
        var thisItem = item;
        var finishValue = parseInt(thisItem.getAttribute('data-count'));
        var stepValue = parseInt(finishValue /  40);

        var interval = setInterval(function(){
          var value = parseInt(thisItem.innerHTML);
          if( value >= finishValue){
            clearInterval(interval);
            thisItem.innerHTML = finishValue;
          }else{
            thisItem.innerHTML = value + stepValue;
          }
        }, 100);

      }
    });
  });
}







/* First Show Animations */
function isScrolledIntoView(el) {
    var rect = el.getBoundingClientRect();
    var elemTop = rect.top;
    var elemBottom = rect.bottom;

    // Only completely visible elements return true:
    var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
    // Partially visible elements return true:
    //isVisible = elemTop < window.innerHeight && elemBottom >= 0;
    return isVisible;
}

'use strict';




/* Vertical process */


var vProcess = document.querySelector('.vertical_process');
if(window.innerWidth > 768 && typeof(vProcess) != 'undefined' && vProcess != null){

  //$(document).ready(function() {
    // Get viewport height, gridTop and gridBottom
    var windowHeight = $(window).height(),
      gridTop = windowHeight * .3,
      gridBottom = windowHeight * .6;



    $(window).on('scroll', function() {
      // On each scroll check if `li` is in interested viewport
      
      $('.vertical_process--item').each(function() {
        var halfWindow = windowHeight / 2;
        //var thisTop = $(this).offset().top - $(window).scrollTop(); // Get the `top` of this `li`

        // Check if this element is in the interested viewport
        //if (thisTop >= gridTop && (thisTop + $(this).height()) <= gridBottom) {
        //if (thisTop >= gridTop) {
          //console.log('$(this).offset().top', $(this).offset().top)
          //console.log('$(window).scrollTop()', $(window).scrollTop())
        if( $(this).offset().top < $(window).scrollTop() + halfWindow){
          $(this).addClass('animated');
          //console.log(11);
        }
      });
    });
  //});



}

































/* Site Preloader -------------------------------- */
document.addEventListener("DOMContentLoaded", function(){
  window.scrollTo(0, 1);
  window.scrollTo(0, 0);
  preloader.style.opacity = "0";
  setTimeout(function(){
    preloader.style.display = "none";
  }, 600)

  if(window.innerWidth > 768){
    document.querySelector('.js-site-header .navbar-brand').style.width = document.querySelector('.js-site-header .btn-primary').getBoundingClientRect().width + 'px';
  }
});


/* Contacts Form -------------------------------- */
var contactsForm = document.getElementById("contactsForm");

if(typeof(contactsForm) != 'undefined' && contactsForm != null){
  // Add filled state
  document.querySelectorAll('.js-form-row .form-control').forEach(item => {
    item.addEventListener('focus', function(){
      this.parentElement.classList.add("is-filled");
    });
  });
  document.querySelectorAll('.js-form-row .form-control').forEach(item => {
    item.addEventListener('blur', function(){
      if(this.value.length == 0){
        this.parentElement.classList.remove("is-filled");
      }
    });
  });

  // Valid email input
  function isEmailValid(email){
   if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
    {
      return (true)
    }
      //alert("You have entered an invalid email address!")
      return (false)
  }

  document.querySelector('#client_email').addEventListener('blur', function(){
    let input = this,
        wrap = this.parentElement,
        notice = this.parentElement.getElementsByClassName("invalid-feedback")[0];
    if( isEmailValid(this.value) ){
      wrap.classList.remove('is-invalid');
      notice.style.display = 'none';
    }else{
      wrap.classList.add('is-invalid');
      notice.style.display = 'block';
    }
  });
  document.querySelector('#client_email').addEventListener('input', function(){
    let input = this,
        wrap = this.parentElement,
        notice = this.parentElement.getElementsByClassName("invalid-feedback")[0];
    wrap.classList.remove('is-invalid');
    notice.style.display = 'none';
  });


  document.querySelector('#customFile').addEventListener('change', function(){
    //var fullPath = document.getElementById('customFile').value;
    let fullPath = this.value;
    if (fullPath) {
        var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
        var filename = fullPath.substring(startIndex);
        if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
            filename = filename.substring(1);
        }
        //alert(filename);
        document.getElementById('customFileTitle').innerHTML = filename;
    }
  });


  // Example starter JavaScript for disabling form submissions if there are invalid fields
  (function () {
    'use strict'

    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation')

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
      .forEach(function (form) {
        form.addEventListener('submit', function (event) {
          event.preventDefault()
          if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
          }

          document.querySelector('.js-submit').getElementsByTagName('span')[0].innerHTML = 'Sending...';
          // TODO: add real request
          setTimeout(function(){
            // Clear all inputs
            document.querySelector('.js-submit').getElementsByTagName('span')[0].innerHTML = 'Success';
            setTimeout(function(){
              document.querySelector('.js-submit').getElementsByTagName('span')[0].innerHTML = 'Send';
            }, 1500);
          }, 2000);

          form.classList.add('was-validated')

          return false;

        }, false)
      })
  })();
  
}

var LOGOclientRect = document.querySelector('.navbar-brand').getBoundingClientRect();
var TogglerclientRect = document.querySelector('.navbar-toggler').getBoundingClientRect();


// For Mobile Full Screen Navigation Menu
document.querySelector('.navbar-toggler').addEventListener('click', function(){
  window.scrollTo(0,0);
  if (document.body.classList.contains('modal-open')) {
    document.body.classList.remove('modal-open');
    document.getElementsByTagName('html')[0].style.overflowY = 'initial';
    document.querySelector('.navbar-brand').style.position = "";
    document.querySelector('.navbar-brand').style.left = "";
    document.querySelector('.navbar-brand').style.top = "";
    document.querySelector('.navbar-brand').style.zIndex = "";

    document.querySelector('.navbar-toggler').style.position = "";
    document.querySelector('.navbar-toggler').style.left = "";
    document.querySelector('.navbar-toggler').style.top = "";
    document.querySelector('.navbar-toggler').style.zIndex = "";

    document.querySelector('.navbar-toggler-icon-close').style.display = "none";
    document.querySelector('.navbar-toggler-icon-humb').style.display = "block";
  }else{
    document.body.classList.add('modal-open');
    document.getElementsByTagName('html')[0].style.overflowY = 'hidden';

    document.querySelector('.navbar-toggler-icon-close').style.display = "block";
    document.querySelector('.navbar-toggler-icon-humb').style.display = "none";
    
    setTimeout(function(){
      document.querySelector('.navbar-brand').style.position = "fixed";
      document.querySelector('.navbar-brand').style.left = LOGOclientRect.x + "px";
      document.querySelector('.navbar-brand').style.top = LOGOclientRect.y + "px";
      document.querySelector('.navbar-brand').style.zIndex = "111111";

      document.querySelector('.navbar-toggler').style.position = "fixed";
      document.querySelector('.navbar-toggler').style.left = TogglerclientRect.x + "px";
      document.querySelector('.navbar-toggler').style.top = TogglerclientRect.y + "px";
      document.querySelector('.navbar-toggler').style.zIndex = "111111";

    }, 600);
  }
});

document.querySelector('header .btn-primary').addEventListener('click', function(){
  if(window.innerWidth < 768){
    document.querySelector('.navbar-toggler').click();
    document.body.classList.add('modal-open');
  }
});



var stickyHeader = document.querySelector(".js-site-header");

if(typeof(stickyHeader) != 'undefined' && stickyHeader != null){
  // Sticky Header Desktop
  window.addEventListener('scroll', function(e) {

    //console.log(window.scrollY);
    //console.log( document.querySelector('.js-site-header .navbar').getBoundingClientRect().top );
    if( document.querySelector('.js-site-header .navbar').getBoundingClientRect().top < 0){
      document.querySelector('.js-site-header').classList.add('fixed-top');
    }
    if( window.scrollY < 36 ){
      document.querySelector('.js-site-header').classList.remove('fixed-top');
    }
    

  });
}





/* First Show Animations */
function isScrolledIntoView(el) {
    var rect = el.getBoundingClientRect();
    var elemTop = rect.top;
    var elemBottom = rect.bottom;

    // Only completely visible elements return true:
    var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
    // Partially visible elements return true:
    //isVisible = elemTop < window.innerHeight && elemBottom >= 0;
    return isVisible;
}








/* Home Page Animations */

var homePage = document.getElementById("homePage");

if(typeof(homePage) != 'undefined' && homePage != null){
  
  window.addEventListener('scroll', function(e) {

    if( isScrolledIntoView( document.querySelector('.hero .preheader') ) ){
      document.querySelector('.hero').classList.add('showed');
    }


    document.querySelectorAll('.slideUpAnim').forEach(item => {
      if( isScrolledIntoView(item) ){
        let delay = item.dataset.delay || 0;
        if (window.innerWidth > 991 && item.dataset.delaydesktop) {
          delay = item.dataset.delaydesktop || 0;
        }
        setTimeout(function(){
          item.classList.add('showed');
        }, delay )
      }
    });
    document.querySelectorAll('.slideUpAnim2').forEach(item => {
      if( isScrolledIntoView(item) ){
        let delay = item.dataset.delay || 0;
        if (window.innerWidth > 991 && item.dataset.delaydesktop) {
          delay = item.dataset.delaydesktop || 0;
        }
        setTimeout(function(){
          item.classList.add('showed');
        }, delay )
      }
    });

    document.querySelectorAll('.slideCopyAnim').forEach(item => {
      if( isScrolledIntoView(item) ){
        let delay = item.dataset.delay || 0;
        if (window.innerWidth > 991 && item.dataset.delaydesktop) {
          delay = item.dataset.delaydesktop || 0;
        }
        setTimeout(function(){
          item.classList.add('showed');
        }, delay )
      }
    });

    document.querySelectorAll('.fadeInAnim').forEach(item => {
      if( isScrolledIntoView(item) ){
        let delay = item.dataset.delay || 0;
        if (window.innerWidth > 991 && item.dataset.delaydesktop) {
          delay = item.dataset.delaydesktop || 0;
        }
        setTimeout(function(){
          item.classList.add('showed');
        }, delay )
      }
    });




    document.querySelectorAll('section.cta .animWrap').forEach(item => {
      if( isScrolledIntoView(item) ){
        let delay = item.dataset.delay || 100;
        if (window.innerWidth > 991 && item.dataset.delaydesktop) {
          delay = item.dataset.delaydesktop || 100;
        }
        setTimeout(function(){
          item.classList.add('showed');
        }, delay )
      }
    });




  });

}







/* About Page Animations */

var aboutPage = document.getElementById("aboutPage");

if(typeof(aboutPage) != 'undefined' && aboutPage != null){
  window.addEventListener('scroll', function(e) {
    
    document.querySelectorAll('.slideUpAnim').forEach(item => {
      if( isScrolledIntoView(item) ){
        let delay = item.dataset.delay || 0;
        if (window.innerWidth > 991 && item.dataset.delaydesktop) {
          delay = item.dataset.delaydesktop || 0;
        }
        setTimeout(function(){
          item.classList.add('showed');
        }, delay )
      }
    });
    
    document.querySelectorAll('.slideRightAnim').forEach(item => {
      if( isScrolledIntoView(item) ){
        let delay = item.dataset.delay || 0;
        if (window.innerWidth > 991 && item.dataset.delaydesktop) {
          delay = item.dataset.delaydesktop || 0;
        }
        setTimeout(function(){
          item.classList.add('showed');
        }, delay )
      }
    });
    
    document.querySelectorAll('.fadeInAnim').forEach(item => {
      if( isScrolledIntoView(item) ){
        let delay = item.dataset.delay || 0;
        if (window.innerWidth > 991 && item.dataset.delaydesktop) {
          delay = item.dataset.delaydesktop || 0;
        }
        setTimeout(function(){
          item.classList.add('showed');
        }, delay )
      }
    });
    
  });
}





// Trigger window scroll after page load
var e = document.createEvent('Event'); 
e.initEvent("scroll", true, true);  
window.dispatchEvent(e);





/* Single Project Slick Carousel */

var projectCarousel = document.getElementById("projectCarousel");

if(typeof(projectCarousel) != 'undefined' && projectCarousel != null){
  
  $('.slick-carousel').slick({
    autoplay: true,
    autoplaySpeed: 2000,
    centerMode: true,
    centerPadding: '200px',
    slidesToShow: 3,
    arrows: false,
    responsive: [
    {
      breakpoint: 991,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
  });
  
}





/* onScroll Show Animations */

var slideRightAnim_Elements = document.querySelectorAll('.slideRightAnim');

if(typeof(slideRightAnim_Elements) != 'undefined' && slideRightAnim_Elements != null){
  window.addEventListener('scroll', function(e) {
    document.querySelectorAll('.slideRightAnim').forEach(item => {
      if( isScrolledIntoView(item) ){
        let delay = item.dataset.delay || 0;
        if (window.innerWidth > 991 && item.dataset.delaydesktop) {
          delay = item.dataset.delaydesktop || 0;
        }
        setTimeout(function(){
          item.classList.add('showed');
        }, delay )
      }
    });
  });
}


var slideRightAnim_Elements = document.querySelectorAll('.slideCopyAnim');

if(typeof(slideRightAnim_Elements) != 'undefined' && slideRightAnim_Elements != null){
  window.addEventListener('scroll', function(e) {
    document.querySelectorAll('.slideCopyAnim').forEach(item => {
      if( isScrolledIntoView(item) ){
        let delay = item.dataset.delay || 0;
        if (window.innerWidth > 991 && item.dataset.delaydesktop) {
          delay = item.dataset.delaydesktop || 0;
        }
        setTimeout(function(){
          item.classList.add('showed');
        }, delay )
      }
    });
  });
}




/* You Win */

var YouWin = document.getElementById("YouWin");

if(typeof(YouWin) != 'undefined' && YouWin != null){
  window.addEventListener('scroll', function(e) {
    if( isScrolledIntoView(YouWin) ){
      
      audioElement.play();
    }
  });
  
  var audioElement = new Audio('./youWin.mp3');
  //audioElement.play();

  audioElement.addEventListener('loadeddata', () => {
    let duration = audioElement.duration;
    // The duration variable now holds the duration (in seconds) of the audio clip 
    audioElement.play();
  })
}








/* Custom Cursor CTA */

var cursorButton = document.getElementById("cursorButton");

if(typeof(cursorButton) != 'undefined' && cursorButton != null){
  var ctaCursor = document.querySelectorAll('.cta-cursor');

  var myModalEl = document.getElementById('contactsModal');

  const onMouseMove = (e) =>{
    cursorButton.style.left = e.pageX + 'px';
    cursorButton.style.top = e.pageY + 'px';
  }
  document.addEventListener('mousemove', onMouseMove);
  
  for(var ctaCursorItem in ctaCursor){
   let color = ( ctaCursor[ctaCursorItem].dataset.noaction === "true" ) ? '0' : ctaCursor[ctaCursorItem].dataset.bgcolor;
   let fontSize = ( ctaCursor[ctaCursorItem].dataset.noaction === "true" ) ? '0' : '';
   ctaCursor[ctaCursorItem].onmouseover=function(){
    cursorButton.classList.remove('is-hidden2');
    //alert( color );
    if(color == 0){
      cursorButton.classList.add('is-noactive');
    }else{
      cursorButton.style.backgroundColor = color;
    }
    cursorButton.style.fontSize = fontSize;
   }
   ctaCursor[ctaCursorItem].onmouseout=function(){
    if(color == 0){
      cursorButton.classList.remove('is-noactive');
    }
    cursorButton.classList.add('is-hidden2');
   }

   if( ctaCursor[ctaCursorItem].dataset.noaction !== "true" ){
     
     ctaCursor[ctaCursorItem].onclick=function(){
       //alert("Contactform")
       document.querySelector('.js-site-header .btn-primary').click();
     }
   }
  }

  

}





/* Projects */

var project = document.querySelector('.projects');

// Hide scrollbar on desktop
if(window.innerWidth > 768 && typeof(project) != 'undefined' && project != null){
  window.addEventListener("load", function(){
    setTimeout(function(){
      document.querySelectorAll('.projects-wrap').forEach(function(item){
        item.style.maxHeight = (item.offsetHeight - 20) + 'px';
      });
    }, 1000);
  });
}





var worksPage = document.querySelector('#worksPage');
if(typeof(worksPage) != 'undefined' && worksPage != null){

  $('.js-filter-all').on('click', function(e){
    e.preventDefault();
    $('section.works-filter a').removeClass('is-active');
    $(this).addClass('is-active');
    $('.js-works-web').fadeIn();
    $('.js-works-app').fadeIn();
    $('.js-works-graphic').fadeIn();
    $('hr').show()
  });
  $('.js-filter-web').on('click', function(e){
    e.preventDefault();
    $('section.works-filter a').removeClass('is-active');
    $(this).addClass('is-active');
    $('.js-works-app').hide();
    $('.js-works-graphic').hide();
    $('.js-works-web').fadeIn();
    $('hr').hide()
  });
  $('.js-filter-app').on('click', function(e){
    e.preventDefault();
    $('section.works-filter a').removeClass('is-active');
    $(this).addClass('is-active');
    $('.js-works-web').hide();
    $('.js-works-graphic').hide();
    $('.js-works-app').fadeIn();
    $('hr').hide()
  });
  $('.js-filter-graphic').on('click', function(e){
    e.preventDefault();
    $('section.works-filter a').removeClass('is-active');
    $(this).addClass('is-active');
    $('.js-works-web').hide();
    $('.js-works-app').hide();
    $('.js-works-graphic').fadeIn();
    $('hr').hide()
  });

}


/*window.addEventListener("load", function(){
  window.addEventListener('scroll', function(e) {
    if( window.scrollY >= a.offsetTop && ){
      //document.querySelector('html').style.overflow = 'hidden';
    }else{

    }
  });
});*/




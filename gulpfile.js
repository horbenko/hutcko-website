'use strict';

const dir = {
        src: 'src/',
        build: './'
      };

var gulp = require('gulp');
var sass = require('gulp-sass'),
    postcss       = require('gulp-postcss'),
    gutil         = require('gulp-util'),
    pug           = require('gulp-pug');

var browserSync = require('browser-sync').create();


// ----------------------------------------------------------------------------------------------------------------

// https://medium.com/swlh/setting-up-gulp-4-0-2-for-bootstrap-sass-and-browsersync-7917f5f5d2c5


//compile scss into css
function style() {
  return gulp.src('scss/**/*.scss')
  .pipe(sass().on('error',sass.logError))
  .pipe(gulp.dest('css'))
  .pipe(browserSync.stream());
}

//compile pug into html
function templates() {
  return gulp.src('pug/*.pug')
  .pipe(pug({
    doctype: 'html',
    pretty: false
  }))
  .pipe(gulp.dest('./'))
  .pipe(browserSync.stream());
}

function watch() {
    browserSync.init({
        server: {
           baseDir: "./",
           index: "/index.html"
        }
    });
    gulp.watch('pug/**/*.pug', templates)
    gulp.watch('scss/**/*.scss', style)
    gulp.watch('./*.html').on('change',browserSync.reload);
    gulp.watch('./js/**/*.js').on('change', browserSync.reload);
}

exports.watch = watch;
exports.style = style;
exports.templates = templates;
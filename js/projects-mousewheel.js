window.scrollTo(0,0);
window.addEventListener("load", function(){
  window.scrollTo(0,0);
});

document.querySelector('body').addEventListener("mousewheel", MouseWheelHandler, false);
document.querySelector('body').addEventListener("DOMMouseScroll", MouseWheelHandler, false);
document.querySelector('body').addEventListener("onmousewheel", MouseWheelHandler, false);


function MouseWheelHandler(e){

  if( isElementTopScreenCenter( document.getElementById("a") ) && !isElementTopScreenBottom( document.getElementById("a") ) ){
    // cross-browser wheel delta
    var e = window.event || e;
    var delta = - 10 * (Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail))));

    var pst = $('#a').scrollLeft() + delta;

    if (pst < 0) {
        pst = 0;
    //} else if (pst > $('.projects-item').width()) {
      //  pst = $('.projects-item').width();
    }

    $('#a').scrollLeft(pst);
    // console.log( $('#a').scrollLeft() );
  }

  if( isElementTopScreenCenter( document.getElementById("b") )  && !isElementTopScreenBottom( document.getElementById("b") ) ){
    // cross-browser wheel delta
     var e = window.event || e;
    var delta = - 10 * (Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail))));

    var pst = $('#b').scrollLeft() - delta;


    $('#b').scrollLeft(pst);
    // console.log( $('#a').scrollLeft() );
  }

  if( isElementTopScreenCenter( document.getElementById("c") )  && !isElementTopScreenBottom( document.getElementById("c") ) ){
    // cross-browser wheel delta
    var e = window.event || e;
    var delta = - 10 * (Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail))));

    var pst = $('#c').scrollLeft() + delta;

    if (pst < 0) {
        pst = 0;
    //} else if (pst > $('.projects-item').width()) {
      //  pst = $('.projects-item').width();
    }

    $('#c').scrollLeft(pst);
    // console.log( $('#a').scrollLeft() );
  }

  // //if( isElementTopScreenTop( document.getElementById("a")) && ($('#a').scrollLeft() < (a.scrollWidth - 100)) ){
  // if( isElementTopScreenTop( document.getElementById("a")) && ($('#a').scrollLeft() < 600) ){
  //   //document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
  // }




}





function isElementTopScreenTop(element){
  if( window.scrollY >= element.offsetTop ){
    return true;
  }else{
    return false;
  }
}

function isElementTopScreenCenter(element){
  if( (window.scrollY + window.innerHeight / 3 ) >= element.offsetTop ){
    return true;
  }else{
    return false;
  }
}

function isElementTopScreenBottom(element){
  if( window.scrollY >= (element.offsetTop + element.clientHeight) ){
    return true;
  }else{
    return false;
  }
}



// var scroller = document.querySelector('#b');
// if(window.innerWidth > 768 && typeof(scroller) != 'undefined' && scroller != null){


//   var scroller = {};
//   scroller.a = document.getElementById("a");

//   /*if (scroller.a.addEventListener) {
//       scroller.a.addEventListener("mousewheel", MouseWheelHandlerA, false);
//       scroller.a.addEventListener("DOMMouseScroll", MouseWheelHandlerA, false);
//   } else scroller.a.attachEvent("onmousewheel", MouseWheelHandlerA);*/

//   scroller.b = document.getElementById("b");

//   if (scroller.b.addEventListener) {
//       scroller.b.addEventListener("mousewheel", MouseWheelHandlerB, false);
//       scroller.b.addEventListener("DOMMouseScroll", MouseWheelHandlerB, false);
//   } else scroller.b.attachEvent("onmousewheel", MouseWheelHandlerB);


//   scroller.c = document.getElementById("c");

//   if (scroller.c.addEventListener) {
//       scroller.c.addEventListener("mousewheel", MouseWheelHandlerC, false);
//       scroller.c.addEventListener("DOMMouseScroll", MouseWheelHandlerC, false);
//   } else scroller.c.attachEvent("onmousewheel", MouseWheelHandlerC);

//   function MouseWheelHandlerA(e) {

//       // cross-browser wheel delta
//       var e = window.event || e;
//       var delta = - 15 * (Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail))));

//       var pst = $('#a').scrollLeft() + delta;

//       if (pst < 0) {
//           pst = 0;
//       //} else if (pst > $('.projects-item').width()) {
//         //  pst = $('.projects-item').width();
//       }

//       $('#a').scrollLeft(pst);
//       console.log( $('#a').scrollLeft() );

//       return false;
//   }

//   function MouseWheelHandlerB(e) {

//       // cross-browser wheel delta
//       var e = window.event || e;
//       var delta = - 15 * (Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail))));

//       var pst = $('#b').scrollLeft() - delta;

//       // if (pst < 0) {
//       //     pst = 0;
//       // } else if (pst > $('.projects-item').width()) {
//       //     pst = $('.projects-item').width();
//       // }

//       //console.log(pst);
//       $('#b').scrollLeft(pst);

//       return false;
//   }

//   function MouseWheelHandlerC(e) {

//       // cross-browser wheel delta
//       var e = window.event || e;
//       var delta = - 15 * (Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail))));

//       var pst = $('#c').scrollLeft() + delta;

//       if (pst < 0) {
//           pst = 0;
//       //} else if (pst > $('.projects-item').width()) {
//         //  pst = $('.projects-item').width();
//       }

//       $('#c').scrollLeft(pst);

//       return false;
//   }


// }